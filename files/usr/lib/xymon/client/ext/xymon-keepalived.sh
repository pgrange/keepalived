#!/bin/bash

execution_start=$(date +"%s%N")

# nom du test : idem celui entre [] dans le .cfg => apparaitra sur hobbit
BBTEST="keepalived"

# HOBBIT CODES
GREEN_CODE=0
GREEN=green
YELLOW_CODE=2
YELLOW=yellow
RED_CODE=1
RED=red
CLEAR_CODE=3
CLEAR_MSG=clear


if [ -z "$BBDISP" ]; then
    # Debug mode
    logger -t $0 "hobbit variables overriden !"
    BBHOME="/tmp"
    BBDISP="0.0.0.0"
    BB="/bin/echo"
fi

###############
# Xymon utils #
###############

xymon_message=$(mktemp)
check_output=$(mktemp)
hobbit_code=$GREEN_CODE

hobbit_push() {
  local bbcolor=$(code_2_color $hobbit_code)
  local last_change=$(stat $0 --format '%z' 2>/dev/null)
  local execution_end=$(date +"%s%N")
  local duration=$(( ( execution_end - execution_start ) / 1000000 )) # millisecondes
  local signature="
<hr/>
<code style='font-size:.8em;' id='about'>
  $0 derni&egrave;re modification le ${last_change}
  <br/>
  Dur&eacute;e d'&eacute;xecution du script en millisecondes:${duration}
  <br/>
</code>"
  # le saut de ligne ci-dessous pour la variable status est TRES TRES TRES IMPORTANT !!!!!
  $BB $BBDISP "status $MACHINE.$BBTEST $bbcolor `date`
$(cat $xymon_message)
${signature}
"
  rm -f $xymon_message
  rm -f $check_output
}

hobbit_check() {
  local silent_green=false
  if [ "$1" == "--silent_green" ]
  then
    silent_green=true
    shift
  fi
  local check="$*"
  $check > $check_output
  local result=$?
  if [[ "$result" != $GREEN_CODE ]] || ! $silent_green
  then
    format_check_result $check_output $result >> $xymon_message
  fi
  hobbit_code=$(aggregate_code $hobbit_code $result)
  return $result
}


code_2_color() {
  local code=$1
  case $code in
    $GREEN_CODE) echo $GREEN ;;
    $YELLOW_CODE) echo $YELLOW ;;
    $CLEAR_CODE) echo $CLEAR_MSG ;;
    *) echo $RED ;;
  esac
}

aggregate_code() {
  local agregated_code=$GREEN_CODE
  for code in $*
  do
    [ $agregated_code == $RED_CODE ]                         && continue
    [ $code == $RED_CODE ] && agregated_code=$RED_CODE       && continue
    [ $agregated_code == $YELLOW_CODE ]                      && continue
    [ $code == $YELLOW_CODE ] && agregated_code=$YELLOW_CODE && continue
    [ $agregated_code == $CLEAR_CODE ]                       && continue
    [ $code == $CLEAR_CODE ] && agregated_code=$CLEAR_CODE   && continue
    agregated_code=$GREEN_CODE
  done
  echo $agregated_code
}

format_check_result() {
  local check_output_file=$1
  local check_status_code=$2
  local color=$(code_2_color $check_status_code)
  cat << EOF
  <fieldset style='border-color:$color; margin:-1em;'>
   <pre>
    $(cat $check_output_file)
   </pre>
  </fieldset>
EOF
}

####################################
# PLACEZ LE CODE DE VOTRE TEST ICI #
####################################

keepalived_state_file=/var/run/keepalived/state
keepalived_state=$(head -n 1 $keepalived_state_file)
keepalived_conf=/etc/keepalived/keepalived.conf

keepalived_is_alive() {
  if [ "`systemctl is-active keepalived`" != "active" ]
  then
    echo "Le service keepalived n'est pas actif"
    systemctl status keepalived
    return $RED_CODE
  else
    echo "Le service keepalived est actif"
    return $GREEN_CODE
  fi
}

what_is_node_state() {
  if [ "$keepalived_state" == "MASTER" ]
  then
    echo "Le noeud est MASTER"
    return $GREEN_CODE
  elif [ "$keepalived_state" == "BACKUP" ]
  then
    echo "Le noeud est BACKUP"
    return $CLEAR_CODE
  else
    echo "Le noeud est dans l'état ${keepalived_state}"
    return $RED_CODE
  fi
}

node_sees_neighbors() {
  # On extrait de la conf tout ce qui est dans la section "unicast_peer { ... }"
  peers=`grep -Pzo "\s*unicast_peer\s*{(\s|\n)*\K([^}]|\n)*(?=\n\s*})" $keepalived_conf | sed -e "s/^[ \t]*//"`
  # Pour chaque noeud, on vérifie que
  # On pourra faire ça le jour où on saura contrôler les communications VRRP ou que keepalived fournira les contrôles suffisants
}

node_master_owns_all_vip() {
  # On extrait de la conf tout ce qui est dans la section "virtual_ipaddress_excluded { ... }", le sed supprime les espaces en début et fin de ligne
  # le changement de IFS, c'est pour que 1 ligne = 1 entrée du tableau (traitement des espaces)
  IFS_bak=$IFS
  IFS=$'\n'
  vips=($(grep -Pzo "\s*virtual_ipaddress_excluded\s*{(\s|\n)*\K([^}]|\n)*(?=\n\s*})" $keepalived_conf | sed -e "s/^[ \t]*//" -e "s/[ \t]*$//"))
  IFS=$IFS_bak
  
  # On vérifie que les VIPs sont activées sur l'OS
  vips_ok=()
  vip_ko=()
  for vip in "${vips[@]}"
  do
  # Avant de tester la présence de l'IP, on s'assure que la ligne lue correspond à une IP ! (e.g. ça pourrait être un commentaire)
  if [[ $vip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
  then
    # Maintenant on teste si la VIP est Ok ou KO
    if (ip addr show | grep -q $vip)
    then
      vips_ok+=($vip)
    else
      vips_ko+=($vip)
    fi
  fi
  done
  
  # On affiche les VIP Ok
  if [ ${#vips_ok[@]} -ne 0 ]; then
    echo "${#vips_ok[@]} VIP Ok"
    for vip in "${vips_ok[@]}"
    do
      echo $vip
    done | sort
  else
    echo "Aucune VIP Ok !"
  fi
  
  # On affiche les VIP KO
  # Et on définit le code de retour
  if [ ${#vips_ko[@]} -ne 0 ]; then
    echo ""
    echo "${#vips_ko[@]} VIP KO"
    for vip in "${vips_ko[@]}"
    do
      echo $vip
    done | sort
    return $RED_CODE
  else
    return $GREEN_CODE
  fi
}

if hobbit_check --silent_green keepalived_is_alive
then
  hobbit_check what_is_node_state
  if [ "$keepalived_state" == "MASTER" ]
  then
    hobbit_check node_master_owns_all_vip
  fi
fi

hobbit_push


